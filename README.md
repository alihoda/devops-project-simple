# DevOps-Project-Simple

A simple devops project which contains (In progress):

- A simple *Go* app
- CI/CD

  - *Gitlab*
  
- Configuration Management

  - *Ansible*

- Container Orchestration

  - *Docker*
  - *Kubernetes*

    > To configure pipeline for kubernetes, [check here](https://gitlab.com/alihoda/documents/-/blob/main/cicd/gitlab/deploy-k8s.md).

- Monitoring

  - *Prometheus*
  - *Grafana*
